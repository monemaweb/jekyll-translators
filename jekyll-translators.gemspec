
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "jekyll-translators/version"

Gem::Specification.new do |spec|
  spec.name          = "jekyll-translators"
  spec.version       = Jekyll::Translators::VERSION
  spec.authors       = ["Alessandro GAddini"]
  spec.email         = ["alessandro@monema.it"]

  spec.summary       = %q{Translators Jekyll plugin}
  spec.description   = %q{Translators Jekyll plugin.}
  spec.homepage      = "https://monema.it/"

  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r!^bin/!) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r!^(test|spec|features)/!)
  spec.require_paths = ["lib"]

  #spec.add_runtime_dependency "jekyll"

  spec.add_dependency "rest-client"
  spec.add_dependency "json"
  spec.add_dependency "slugify"

  spec.add_development_dependency "bundler", "~> 1.15"
  spec.add_development_dependency "rake", "~> 10.0"
end
