require 'rest-client'
require 'json'
require 'yaml'
require 'base64'
require 'fileutils'
require 'slugify'
require 'open-uri'

module Jekyll
  # Module for Jekyll Commands
  module Commands
    # jekyll translators Command
    class Translators < Command
      def self.init_with_program(prog)
        prog.command(:translators) do |c|
          c.syntax 'translators [OPTIONS]'
          c.description 'Imports data from Translators'

          options.each { |opt| c.option(*opt) }

          add_build_options(c)

          command_action(c)
        end
      end

      def self.options
        [
          [
            'configuration', '-c', '--configuration',
            'Build Translators Plugin Configuration file'
          ],
          [
            'rebuild', '-r', '--rebuild',
            'Rebuild Jekyll Site after fetching data'
          ],
          [
            'apikey', '--translators-apikey',
            'Set the Translators Plugin! ApiKey'
          ],
          [
            'apiurl', '--translators-endpoint',
            'Set the Translators Plugin! Endpoint url'
          ],
          [
            'dateFrom', '--date-from',
            'Set the Translators Plugin! Endpoint url'
          ]
        ]
      end

      def self.command_action(command)
        command.action do |args, options|
          jekyll_options = configuration_from_options(options)
          translators_config = jekyll_options['translators']
          process args, options, translators_config
        end
      end

      def self.build_configuration(_args = [], options = {}, translators_config = {})
        Jekyll.logger.info 'Building configuration file'

        url = translators_config['apiurl'] + '/api/sites/' + translators_config['site'] + '/configuration'
        # auth = 'Bearer ' + Base64.encode64( translators_config['apikey'] ).chomp
        auth = 'Bearer ' + translators_config['apikey'];

        @resource = RestClient::Resource.new(url)
        @response = @resource.get( :Authorization => auth )
        response_hash = JSON.parse(@response)

	File.open('_translators.yaml.example', 'w') {|f| f.write response_hash.to_yaml }

        Jekyll.logger.info 'Configuration file created: _translators.yaml.example'
        Jekyll.logger.info 'Rename to _translators.yaml before use'
      end

      def self.import_entries(_args = [], options = {}, translators_config = {})
        Jekyll.logger.info 'Importing data files'
        FileUtils.mkdir_p '_translators' unless File.directory?('_translators')

        url = translators_config['apiurl'] + '/api/v1/entries?fromDate=' + translators_config['dateFrom']
        auth = 'Bearer ' + translators_config['apikey']

        @resource = RestClient::Resource.new(url)
        @response = @resource.get( :Authorization => auth )
        response_hash = JSON.parse(@response)

        return response_hash

      end

      def self.import_entry(_args = [], options = {}, translators_config = {}, _id)
        Jekyll.logger.info 'Import single entry ' + _id

        url = translators_config['apiurl'] + '/api/v1/entries/' + _id
        auth = 'Bearer ' + translators_config['apikey']

        @datafile_resource = RestClient::Resource.new(url)
        @datafile_response = @datafile_resource.get( :Authorization => auth )
        entry_hash = JSON.parse(@datafile_response)

        if entry_hash['partA']['title'] and entry_hash['partA']['lang']
          filename = entry_hash['partA']['lang'] + '_' + entry_hash['partA']['title']
          pathfile = '_translators/' + filename.slugify + '.md';

          if !entry_hash['partA']['author']
            entry_hash['partA']['author'] = { "name" => "", "surname" => "" }
          end

          if entry_hash['translations']
            entry_hash['translations'].each do |translation|
              translation['title'] = translation['title'].slugify
            end
          end

          entry_parsed = YAML.dump(entry_hash)

          File.open(pathfile, 'w') do |d|
            d.puts entry_parsed
            d.puts 'permalink: /translators/' +  filename.slugify + '.html'
            d.puts 'indexcategory: ' + entry_hash['partA']['title'][0].upcase
            d.puts 'layout: scheda'
            d.puts '---'
          end
        end
      end

      def self.import_mediafiles(_args = [], options = {}, translators_config = {})
        Jekyll.logger.info 'Importing media files'

        url = translators_config['apiurl'] + '/api/sites/' + translators_config['site'] + '/media'
        auth = 'Bearer ' + translators_config['apikey'];

        @resource = RestClient::Resource.new(url)
        @response = @resource.get( :Authorization => auth )
        response_hash = JSON.parse(@response)

        response_hash.each do |media|
          filename = File.join(translators_config['media']['dest'], media['url'].gsub(translators_config['media']['match'], translators_config['media']['replace']))
          Jekyll.logger.info filename

          dirname = File.dirname(filename)

          FileUtils.mkdir_p dirname unless File.directory?(dirname)
          IO.copy_stream(open('http://localhost:3000' + media['url'] ), filename)
        end
      end

      def self.process(_args = [], options = {}, translators_config = {})
        Jekyll.logger.info 'Translators Plugin'

        Jekyll.logger.info 'API Key: ', translators_config['apikey']
        Jekyll.logger.info 'API URL: ', translators_config['apiurl']

        list = import_entries _args, options, translators_config

        list.each do |entry|
          import_entry _args, options, translators_config, entry['_id']
        end

        Jekyll.logger.info 'Translators import finished'

        return unless options['rebuild']

        Jekyll.logger.info 'Starting Jekyll Rebuild'
        Jekyll::Commands::Build.process(options)
      end
    end
  end
end
